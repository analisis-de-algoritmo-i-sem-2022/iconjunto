/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author MADARME
 */
public class Test_ConjuntoPar {
    
    public static void main(String[] args) {
        
        unEjemploDeMultiLista();
        
        ListaS<Integer> ejemploLista=new ListaS();
        //Creando una lista de prueba:
        ejemploLista.insertarAlFinal(2);
        ejemploLista.insertarAlFinal(4);
        ejemploLista.insertarAlFinal(5);
        //Llamando el método conjuntoPar:
        ListaS<ListaS<Integer>> par=ejemploLista.getConjuntoPar();
        //Imprimir caso de prueba:
        System.out.println("\n\nMi conjunto par:"+getString_MultiLista(par));
        
    }
    
    private static <T> String getString_MultiLista(ListaS<ListaS<T>> myLista)
    {
    
        if(myLista==null || myLista.esVacia())
            return "MultiLista Vacía";
        
        String msg="{ ";
        for(ListaS<T> myListaInterna: myLista)
        {
            msg+="(";
            for(T elemento:myListaInterna)
            {
            msg+=elemento.toString()+",";
            }
            //Eliminar la última ",";
            msg = msg.substring(0, msg.length() - 1);
            msg+=")\t";
        }
        //Eliminar el último "\t";
        msg = msg.substring(0, msg.length() - 1);
        msg+="}";
        return msg;
    }
    
    private static void unEjemploDeMultiLista()
    {
        
        //Creando la Multilista:
        ListaS<ListaS<Integer>> m=new ListaS();
        
        //Creando la lista1:
        ListaS<Integer> l1=new ListaS();
        l1.insertarAlFinal(1);
        l1.insertarAlFinal(2);
        
        //Creando la lista2:
        ListaS<Integer> l2=new ListaS();
        l2.insertarAlFinal(5);
        l2.insertarAlFinal(8);
        l2.insertarAlFinal(10);
        
        //Insertando estas listas en la Multilista
        m.insertarAlFinal(l1);
        m.insertarAlFinal(l2);
        
        System.out.println("Una multilista de ejemplo:"+getString_MultiLista(m));
    }
    
}
